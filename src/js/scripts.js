
$(document).ready(function () {

    // Randomize carousel items on load

    var $item = $('.carousel-item');

    var $numberofSlides = $('.carousel-item').length;
    var $currentSlide = Math.floor((Math.random() * $numberofSlides));
    if ($currentSlide > 0){
        $('.carousel-item').each(function(){
            $('.carousel-item:nth-child(' + $currentSlide + ')').addClass('active');
        });
    }else{
        $('.carousel-item:last-child').addClass('active');
    }

    // Load data from JSON

    $.getJSON( "data/data.json", function( data ) {
        var items = [];
        $.each( data, function( key, val ) {
            items.push( `
            <figure class="post-item" id="post-` + val.id + `">
                <div class="d-flex">
                    <img class="img-thumbnail mr-3" src="../src/img/posts/` + val.image + `" alt="">
                    <div>
                        <h2>` + val.productName + `</h2>
                        <p class="small text-blue mb-1">Posted: 23/1-2011</p>
                        <div class="body">` + val.body + `</div>
                        <a data-value="` + val.id + `" data-toggle="modal" data-target="#exampleModal" id="post-btn-` + val.id + `" class="btn btn-blue mt-1" href="">Read more</a>
                    </div>
                </div>
            </figure>
            `);
        
        });

        

        $( "<div>", {
            "class": "posts col", "id": "jar",
            html: items.join( "" )
        }).appendTo( ".post-container" );


        // Append data to modal
        $(".post-item a[data-value], .results a[data-value]").on("click", function() {
            var setData = $(this).data('value');
            var title = $("#post-" + setData +" h2").text(); 
            var body = $("#post-" + setData +" .body").text(); 
            $('#exampleModal').find('.modal-title').text(title);
            $('#exampleModal').find('.modal-body').text(body);
            $("#exampleModal").show();
        });
        // Ajax Search on keyUp

        var check = 0;
        var mount = 0;
        
        //Search when key up
        $("#filter").keyup(function(){
            results= [];
            
            // Retrieve the input field text and reset the count to zero
            var filter = $(this).val();
            
            if($('.results-container').css('display') == 'none'){
                $(".results-container").show();
            }

            // Loop through the post list

            $.each( data, function( key, val ) {
                // Check is posts has been mounted
                if (mount == 0 ) {
              
                    // Mount results
                    results.push( "<li id='item-" + key + "'><a href='#' data-toggle='modal' data-target='#exampleModal' data-value='" + val.id + "'>" + val.productName + "</a></li>")
                };
                // If the list item does not contain the text phrase i fade it out
                if (val.productName.search(new RegExp(filter, "i")) < 0 || !results === 0) {
                    $("#item-" + key).fadeOut();
    
                // Show the list item if the phrase matches and increase the count by 1
                } else {
                    $("#item-" + key).show();
                } 
     
            });

            // If results not mounted append them to the results-container
            if (mount == 0 ) {
                $( "<div>", {
                    "class": "results",
                    html: results.join( "" )
                }).appendTo( ".results-container" );
            }
            mount = 1;

       
            $('.results').on('scroll', function() {
                if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    $(this).addClass('active');
                }else{
                    
                    $(this).removeClass('active');
                }
            })
            
        });

        $(".search-icon").on("click", function() {
            results= [];
            $(".results li, .results-container").fadeOut();
            $("#filter").val('');
        });

        $("#nav-btn").on("click", function() {
            $(this).toggleClass('active');
            $('body, html').toggleClass('no-overflow');
            $('#navbarContent').toggleClass('active');
        });

    // Define pagination rules

    function getPageList(totalPages, page, maxLength) {
        if (maxLength < 5) throw "maxLength must be at least 5";
    
        function range(start, end) {
        return Array.from(Array(end - start + 1), (_, i) => i + start);
        }
    
        var sideWidth = maxLength < 9 ? 1 : 2;
        var leftWidth = (maxLength - sideWidth * 2 - 3) >> 1;
        var rightWidth = (maxLength - sideWidth * 2 - 2) >> 1;
        if (totalPages <= maxLength) {
        // no breaks in list
        return range(1, totalPages);
        }
        if (page <= maxLength - sideWidth - 1 - rightWidth) {
        // no break on left of page
        return range(1, maxLength - sideWidth - 1)
            .concat([0])
            .concat(range(totalPages - sideWidth + 1, totalPages));
        }
        if (page >= totalPages - sideWidth - 1 - rightWidth) {
        // no break on right of page
        return range(1, sideWidth)
            .concat([0])
            .concat(
            range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages)
            );
        }
        // Breaks on both sides
        return range(1, sideWidth)
        .concat([0])
        .concat(range(page - leftWidth, page + rightWidth))
        .concat([0])
        .concat(range(totalPages - sideWidth + 1, totalPages));
    }

    $(function() {
        // Number of items and limits the number of items per page
        var numberOfItems = $("#jar .post-item").length;
        var limitPerPage = 3;
        // Total pages rounded upwards
        var totalPages = Math.ceil(numberOfItems / limitPerPage);

        // Must be at least 5:
        var paginationSize = 7;
        var currentPage;

        function showPage(whichPage) {
            if (whichPage < 1 || whichPage > totalPages) return false;
            currentPage = whichPage;
            $("#jar .post-item")
            .hide()
            .slice((currentPage - 1) * limitPerPage, currentPage * limitPerPage)
            .show();
            // Replacing the navigation items
            $(".pagination li").slice(1, -1).remove();
            getPageList(totalPages, currentPage, paginationSize).forEach(item => {
            $("<li>")
                .addClass(
                "page-item " +
                    (item ? "current-page " : "") +
                    (item === currentPage ? "active " : "")
                )
                .append(
                $("<a>")
                    .addClass("page-link")
                    .attr({
                    href: "javascript:void(0)"
                    })
                    .text(item || "...")
                )
                .insertBefore("#next-page");
            });
            return true;
        }

        // Include the prev/next buttons:
        $(".pagination").append(
        $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
            $("<a>")
            .addClass("page-link")
            .attr({
                href: "javascript:void(0)"
            })
            .text("«")
        ),
        $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
            $("<a>")
            .addClass("page-link")
            .attr({
                href: "javascript:void(0)"
            }).text("»"))
        );

        // Show the page links
        $("#jar").show();
        showPage(1);

        $("#count-page").text(currentPage + " out of " + totalPages);
        $('#previous-page').hide();

        function pageDetector() {
            // Detect if lastpage hide next btn
            $("#count-page").text(currentPage + " out of " + totalPages);
            if(currentPage == 1){
                $('#previous-page').hide();
            }else{
                $('#previous-page').show();
            }
            // Detect if lastpage hide next btn
            if(currentPage == totalPages){
                $('#next-page').hide();
            }else{
                $('#next-page').show();
            }
        }

        $(document).on("click", ".pagination li.current-page:not(.active)", function() {
            showPage(+$(this).text());        
            pageDetector();
        });
 

        $("#next-page").on("click", function() {
            $('#previous-page').show();
            return showPage(currentPage + 1);     
        });

        $("#previous-page").on("click", function() {
            return showPage(currentPage - 1);
        }); 
        
        $("#next-page, #previous-page, pagination li a").on("click", function() {
            pageDetector();
        });
    });
});

    $.getJSON( "data/sponsors-data.json", function( data ) {
        var brands = [];
        $.each( data, function( key, val ) {
            brands.push( `<div class="brand-item img-thumbnail "><img src="../src/img/brands/` + val.image + `" alt="` + val.productName + `"></div>`);
        });

        $( "<div>", {
            "class": "brands row",
            html: brands.join( "" )
        }).appendTo( ".brands-container" );
    });

    var ul = $('#flickr');
    var url = 'http://api.flickr.com/services/feeds/photos_public.gne?id=19842098@N00&format=json&jsoncallback=?';
    imgs = '';

    $.getJSON(url, function(data) {
        imgs = data;
        $(data.items).each(function(i, item) {
            ul.append($('<div class="fl-item " style="background-image:url(' + item.media.m.slice(0,-5) + 's.jpg)"></div>'))
            if (i == 8) return false
        });
    })   
    

    // Display up button when past 620px;
    $(function(){
        var shrinkHeader = 620;
        $(window).scroll(function() {
            var scroll = getCurrentScroll();
            if ( scroll >= shrinkHeader ) {
                $('#to-top').addClass('active');
            }
            else {
                $('#to-top').removeClass('active');
            }
        });
        function getCurrentScroll() {
            return window.pageYOffset || document.documentElement.scrollTop;
        }
    });

    // Animate to top on click
    $('#to-top').on('click', function(){
        $("html, body").stop().animate({scrollTop:0}, 500, 'swing');
    });

});