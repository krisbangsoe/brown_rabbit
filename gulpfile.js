
var gulp = require('gulp');
var sass = require('gulp-sass');
// var imagemin = require('gulp-imagemin');

gulp.task('sass', function () {
return gulp.src('./src/sass/*')
	.pipe(sass.sync().on('error', sass.logError))
	.pipe(gulp.dest('./src/css'));
});

gulp.task('sass:watch', function () {
gulp.watch('./src/sass/*', ['sass']);
});

/*
gulp.task('default', function () {
    gulp.src('images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'));
});
*/
